# Infinite Grimm

Infinite Grimm is a hollow knight mod supporting lifeblood. It allows you to fight NKG a second time in an increasingly difficult endless battle for geo and fun. How long can you dance with fire?

### Features

* Fight Grimm in an endless, increasingly difficult battle.
* Earn geo and compliments from Grimm based on the damage done.
* Optional hard mode for really skilled players.
* Every attack dodgeable at every speed. No more relying on RNG for your hitless run.*
* Bring Grimmchild with you for moral support and extra damage.
* Leave Grimmchild behind for two extra notches.
* Rewrites Hollow Knight lore.


\*Assumes every movement ability unlocked.

### How to install

This mod depends on:

[ModCommon](https://github.com/Kerr1291/ModCommon) by Kerr1291

[Modding API](https://github.com/seanpr96/HollowKnight.Modding) by Wyza, Firzen, and Seanpr

So first install these from either github or the google drive.

Then download or compile the Infinite Grimm dll from the "releases" section.

For meaningful progression, it's worth installing this mod alongside [Grimmchild Upgrades](https://github.com/natis1/grimmchildupgrades)

---

Note about copyright:

All files with the exception of PlayMakerFSMExtensions.cs are copyright under the Gnu Public License Version 3 (see LICENSE).

FsmUtil originally by KDT and obtained from 56's Mantis God's repository. Used in accordance with the Gnu Public License Version 3

PlayMaker FSM Extensions by Seanpr from Randomizer 2.0 mod. Used with permission. Because of the terms of the GPLv3, this file is also licensed under these terms.

---

Special thanks: Basically everyone who has ever made a hollow knight mod for helping me out with my first C# project. This includes, but is not limited to, people I stole code from like: KDT, Gradow, Seanpr, 56, and Firzen.
