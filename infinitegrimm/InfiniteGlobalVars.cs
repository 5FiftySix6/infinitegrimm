﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace infinitegrimm
{
    public class InfiniteGlobalVars
    {
        public static readonly string version = "0.3.0";
        public static readonly int loadOrder = 25;
        public static int maximumDamage;
        public static readonly int versionInt = 300;
    }
}
